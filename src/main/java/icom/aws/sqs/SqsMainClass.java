package icom.aws.sqs;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.*;
import icom.aws.CredUtils;
import icom.aws.DynamoDbCreds;

import java.io.IOException;
import java.util.stream.Collectors;

public class SqsMainClass {

    public static final String QUEUE_URL = "https://sqs.eu-central-1.amazonaws.com/038650850710/demo-queue";
    public static final String QUEUE_NAME = "demo-queue";

    public static void main(String[] args) throws IOException {
        DynamoDbCreds credentials = CredUtils.getCredentials();
        AmazonSQS sqsService = AmazonSQSClientBuilder
                .standard()
                .withCredentials(
                        new AWSStaticCredentialsProvider(
                                new BasicAWSCredentials(credentials.getAccessKey(), credentials.getSecretKey())))
                .withRegion(Regions.EU_CENTRAL_1)
                .build();

        createQueueRequest(sqsService);

        sendMessage(sqsService);

        sendMessageWithRequest(sqsService);

        deleteFirstMessage(sqsService);
    }

    private static void deleteFirstMessage(AmazonSQS sqsService) {
        ReceiveMessageResult receiveMessageResult = sqsService.receiveMessage(new ReceiveMessageRequest()
                .withQueueUrl(QUEUE_URL)
                .withWaitTimeSeconds(2)
                .withMessageAttributeNames());
        String firstMessageReceiptHandle = receiveMessageResult.getMessages().get(0).getReceiptHandle();
        DeleteMessageResult deleteMessageResult = sqsService.deleteMessage(QUEUE_URL, firstMessageReceiptHandle);
        System.out.println("Result of delete request:\n" + deleteMessageResult.toString());
    }

    private static void sendMessageWithRequest(AmazonSQS sqsService) {
        SendMessageResult messageResult = sqsService.sendMessage(
                new SendMessageRequest()
                        .withQueueUrl(QUEUE_URL)
                        .withMessageBody("message from queue request")
                        .withDelaySeconds(4))
                .withMessageId("custom-message-ID");
        System.out.println("\nResponse after request send message executed:\n"
                + messageResult.getMessageId() + "\n" + messageResult.getMD5OfMessageBody());
    }

    private static void sendMessage(AmazonSQS sqsService) {
        System.out.println("\nQueue messages before send message request\n" +
                sqsService.receiveMessage(QUEUE_NAME).getMessages()
                        .stream().map(Message::getBody).collect(Collectors.toList()));
        SendMessageResult sendMessageResult = sqsService.sendMessage(QUEUE_NAME, "second message");
        System.out.println("Result of sent message: " + sendMessageResult.toString());
        System.out.println("\nQueue messages after send message request\n" +
                sqsService.receiveMessage(QUEUE_NAME).getMessages()
                        .stream().map(Message::getBody).collect(Collectors.toList()));
    }

    private static void createQueueRequest(AmazonSQS sqsService) {
        CreateQueueResult queue = sqsService.createQueue(QUEUE_NAME);
        System.out.println("Queue creation response with:\n"
                + queue.toString()
                + queue.getSdkHttpMetadata().getAllHttpHeaders().get("Date"));
    }
}
