package icom.aws;

import com.amazonaws.util.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;

public class CredUtils {
    public static DynamoDbCreds getCredentials() throws IOException {
        String aws_home = System.getenv("aws_home");
        if (aws_home == null) {
            System.out.println("aws credentials path not found");
            throw new NullPointerException("path is null");
        }

        File credentialFile = new File(aws_home + "\\credentials");
        FileInputStream fileInputStream;
        fileInputStream = new FileInputStream(credentialFile);
        String[] lines = IOUtils.toString(fileInputStream).split(System.lineSeparator());
        return new DynamoDbCreds(getKeyByNameFromLines(lines, "aws_access_key_id"),
                getKeyByNameFromLines(lines, "aws_secret_access_key"));
    }

    private static String getKeyByNameFromLines(String[] lines, String keyName) {
        return Arrays.stream(lines)
                .filter(s -> s.startsWith(keyName + "="))
                .flatMap(s -> Arrays.stream(s.split(keyName + "=")))
                .filter(s -> !s.isEmpty())
                .findFirst().orElseThrow(() -> new NullPointerException("value mustn't be null"));
    }
}
