package icom.aws.s3;

import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import icom.aws.CredUtils;
import icom.aws.DynamoDbCreds;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class S3MainClass {

    public static void main(String[] args) throws IOException {
        final AmazonS3 s3 = getAmazonS3();
        String bucketName = "test-bucket-2020-12-23-v2";
        String fileName = "testFileName.txt";

        System.out.println("Existed buckets: \n"
                + s3.listBuckets().stream().map(Bucket::getName).collect(Collectors.toList()));

        createBucketByName(s3, bucketName);

        System.out.println("Existed buckets: \n"
                + s3.listBuckets().stream().map(Bucket::getName).collect(Collectors.toList()));

        uploadTestFile(s3, bucketName, fileName);
        checkTestFileInsideS3(s3, bucketName, fileName);
        removeFile(s3, bucketName, fileName);
    }

    private static void removeFile(AmazonS3 s3, String bucketName, String fileName) {
        System.out.println("Keys objects before remove:\n"
                + s3.listObjects(bucketName).getObjectSummaries()
                .stream().map(S3ObjectSummary::getKey).collect(Collectors.toList()));
        s3.deleteObject(bucketName, fileName);
        System.out.println("Keys objects after remove:\n"
                + s3.listObjects(bucketName).getObjectSummaries()
                .stream().map(S3ObjectSummary::getKey).collect(Collectors.toList()));
    }

    private static void checkTestFileInsideS3(AmazonS3 s3, String bucketName, String fileName) {
        S3Object object = s3.getObject(new GetObjectRequest(bucketName, fileName));
        try (S3ObjectInputStream objectContent = object.getObjectContent()) {
            BufferedReader bufferedReader =
                    new BufferedReader(
                            new InputStreamReader(objectContent.getDelegateStream()));
            System.out.println("\nText from file:");
            String line;
            while( (line = bufferedReader.readLine()) != null){
                System.out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void uploadTestFile(AmazonS3 s3, String bucketName, String fileName) {
        System.out.println("Keys objects before upload:\n"
                + s3.listObjects(bucketName).getObjectSummaries()
                .stream().map(S3ObjectSummary::getKey).collect(Collectors.toList()));
        s3.putObject(bucketName, fileName, new File("src/main/resources/test.txt"));
        System.out.println("file uploaded");
    }

    private static AmazonS3 getAmazonS3() throws IOException {
        DynamoDbCreds credentials = CredUtils.getCredentials();
        return AmazonS3ClientBuilder
                .standard()
                .withCredentials(
                        new AWSStaticCredentialsProvider(
                                new BasicAWSCredentials(credentials.getAccessKey(), credentials.getSecretKey())
                        ))
                .withRegion(Regions.EU_CENTRAL_1)
                .build();
    }

    private static void createBucketByName(AmazonS3 s3, String bucketName) {
        try {
            s3.createBucket(bucketName);
        } catch (SdkClientException e) {
            System.out.printf("BUCKET %s ALREADY EXISTS%n", bucketName);
        }
    }
}
