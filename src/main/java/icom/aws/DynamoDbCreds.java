package icom.aws;

public class DynamoDbCreds {
    private final String accessKey;
    private final String secretKey;

    public DynamoDbCreds(String accessKey, String secretKey) {
        this.accessKey = accessKey;
        this.secretKey = secretKey;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }
}
